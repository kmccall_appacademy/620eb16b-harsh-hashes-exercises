# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  lengths = Hash.new(0)
  str.split.each {|word| lengths[word]=word.length}
  lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k,v| v}.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each {|k,v| older[k]=v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  count = Hash.new(0)
  word.chars.each {|ch| count[ch]+=1}
  count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq = Hash.new(0)
  arr.each {|el| uniq[el]+=1}
  uniq.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_n_odd = Hash.new(0)
  numbers.each {|el| (el.even?) ? even_n_odd[:even]+=1 : even_n_odd[:odd]+=1}
  even_n_odd
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  vowels_hash = Hash.new(0)
  string.chars.each {|ch| vowels_hash[ch]+=1 if vowels.include?(ch)}
  vowels_hash.sort_by {|k,v| v}.last.first
end
# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  combinations(students.select {|k,v| (7..12).include?(v)}.keys)
end

def combinations(arr)
  combined = []
  (0..arr.length-2).each do |index_1|
    (index_1+1..arr.length-1).each do |index_2|
      combined << [arr[index_1],arr[index_2]]
    end
  end
  combined
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  biodiversity = Hash.new(0)
  specimens.each {|species| biodiversity[species]+=1}

  number_of_species = biodiversity.length
  min_population = biodiversity.values.min
  max_population = biodiversity.values.max

  index = number_of_species**2 * min_population / max_population
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)

  chars_normal = character_count(normal_sign.downcase)

  chars_vandalized = character_count(vandalized_sign.downcase)

  chars_vandalized.all? do |k,v|
    chars_normal.has_key?(k) && v <= chars_normal[k]
  end

end

def character_count(str)
  count = Hash.new(0)
  str.delete(" .,;:?!\'\"").chars.each {|ch| count[ch]+=1}
  count.sort_by {|k,v| k}.to_h
end
